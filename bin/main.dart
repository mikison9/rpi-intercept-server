import 'package:grpc/grpc.dart';
import 'package:rpi_intercept_server/controller_server.dart';
import 'package:rpi_intercept_server/sslsplit_controller.dart';
import 'package:rpi_intercept_server/utilities.dart';
import 'package:rpi_intercept_server/wifi_controller.dart';

void main(List<String> arguments) async {
  //await SslsplitController.startSslsplit();

  await WifiController.createAP();

  final server = Server([ControllerService()]);
  await server.serve(port: 50051, address: '0.0.0.0');
  myPrint('Server listening on port ${server.port}...');
}
