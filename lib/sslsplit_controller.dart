import 'dart:io';

import 'package:rpi_intercept_server/utilities.dart';

class SslsplitController {
  static Process _currentProcess;

  //sslsplit -c rootCA.crt https 0.0.0.0 8080 -k rootCA.key -L sslsplit.log
  //iptables -t nat -A PREROUTING -p TCP --destination-port 443 -j REDIRECT --to-port 8080
  //iptables -t nat -D PREROUTING -p TCP --destination-port 443 -j REDIRECT --to-port 8080

  static Future<void> startSslsplit() async {
    myPrint('Starting sslsplit');

    await _setupIptables();

    _currentProcess = await Process.start('expect', ['./start_sslsplit']);

    await sleep1000();

    //_currentProcess.stdout.listen((List<int> data) {
    //  myPrint(String.fromCharCodes(data));
    //});

    //_currentProcess?.stdin?.write('heslo1234');

    //myPrint('ssplit return code: ${await _currentProcess.exitCode}');

    await sleep1000();
  }

  static Future<void> stopSslsplit() async {
    myPrint('Stopping sslsplit');
    
    await _clearIptables();

    await _currentProcess.kill();

    await sleep1000();
  }

  static Future<void> _setupIptables() async {
    await Process.run('iptables', [
      '-t',
      'nat',
      '-A',
      'PREROUTING',
      '-p',
      'TCP',
      '--destination-port',
      '443',
      '-j',
      'REDIRECT',
      '--to-port',
      '8080'
    ]);

    await sleep1000();
  }

  static Future<void> _clearIptables() async {
    await Process.run('iptables', [
      '-t',
      'nat',
      '-D',
      'PREROUTING',
      '-p',
      'TCP',
      '--destination-port',
      '443',
      '-j',
      'REDIRECT',
      '--to-port',
      '8080'
    ]);

    await sleep1000();
  }
}
