import 'dart:async';

import 'package:grpc/service_api.dart';
import 'package:pedantic/pedantic.dart';
import 'package:rpi_intercept_proto/src/generated/controller.pb.dart';
import 'package:rpi_intercept_proto/src/generated/controller.pbgrpc.dart';
import 'package:rpi_intercept_server/sslsplit_controller.dart';
import 'package:rpi_intercept_server/utilities.dart';
import 'package:rpi_intercept_server/wifi_controller.dart';

class ControllerService extends ControllerServiceBase {
  @override
  Future<Empty> connect(ServiceCall call, Wifi request) async {
    myPrint('connecting to: ${request.ssid}');
    unawaited(WifiController.bridgeWifi(request.ssid, request.password));
    return Empty();
  }

  @override
  Future<Empty> startExploiting(ServiceCall call, Empty request) async {
    myPrint('starting exploit');
    return Empty();
  }

  @override
  Stream<Host> getConnected(ServiceCall call, Empty request) {
    final stream = StreamController<Host>();

    getConnectedHosts()
        .then((list) => list.forEach((str) => stream.add(Host()..mac = str)));

    return stream.stream;
  }

  @override
  Future<Empty> startSslsplit(ServiceCall call, Empty request) async {
    await SslsplitController.startSslsplit();

    return Empty();
  }

  @override
  Future<Empty> stopSslsplit(
      ServiceCall call, Empty request) async {
    await SslsplitController.stopSslsplit();

    return Empty();
  }
}
