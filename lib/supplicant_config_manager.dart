import 'dart:io';

final String wpaConfigFilename = 'wpa.conf';

class SupplicantConfigManager {
  static void createConfig(String ssid, String password) {
    final toWrite = '''# reading passphrase from stdin
network={
  ssid="$ssid"
  psk="$password"
}
''';

    final file = File(wpaConfigFilename);

    file.writeAsString(toWrite);
  }
}
