import 'dart:io';

import 'package:meta/meta.dart';
import 'package:rpi_intercept_server/supplicant_config_manager.dart';
import 'package:rpi_intercept_server/utilities.dart';

class WifiController {
  static APProcess _currentProcess;

  static String APSSID = 'MyAccessPoint';
  static String APPassword = 'heslo1234';

  static Future<void> createAP() async {
    if (_currentProcess != null) {
      await _currentProcess.stop();
    }

    final newProcess = LocalAP(APSSID, APPassword);
    await newProcess.start();
    _currentProcess = newProcess;
  }

  static Future<void> bridgeWifi(String ssid, String password) async {
    if (_currentProcess != null) {
      await _currentProcess.stop();
    }

    final newProcess = BridgedAP(
        wifiSSID: ssid,
        wifiPassword: password,
        APSSID: APSSID,
        APPassword: APPassword);
    await newProcess.start();
    _currentProcess = newProcess;
  }
}

abstract class APProcess {
  Future<void> stop();
}

class LocalAP extends APProcess {
  final String name;
  final String password;

  //create_ap -n wlan0 MyAccessPoint heslo1234
  Process _process;

  LocalAP(this.name, [this.password]);

  Future<void> start() async {
    _process = await Process.start(
        'create_ap', <String>['-n', 'wlan0', name, password]);

    _process.stdout
        .listen((List<int> data) => myPrint(String.fromCharCodes(data).trim()));
  }

  @override
  Future<void> stop() async {
    await _process?.kill(ProcessSignal.sigint);
    await sleep1000();
  }
}

class BridgedAP extends APProcess {
  final String wifiSSID;
  final String wifiPassword;

  final String APSSID;
  final String APPassword;

  //wpa_supplicant -D nl80211 -i wlan0 -c wpa.conf
  Process _wpa_supplicantProcess;

  //create_ap -m nat wlan0 wlan0 MyAccessPoint heslo1234
  Process _create_apProcess;

  BridgedAP(
      {@required this.wifiSSID,
      @required this.wifiPassword,
      @required this.APSSID,
      @required this.APPassword});

  Future<void> start() async {
    SupplicantConfigManager.createConfig(wifiSSID, wifiPassword);

    _wpa_supplicantProcess = await Process.start(
        'wpa_supplicant', ['-D', 'nl80211', '-i', 'wlan0', '-c', 'wpa.conf']);

    _wpa_supplicantProcess.stdout.listen((List<int> data) {
      myPrint(String.fromCharCodes(data).trim());
    });

    await Process.run('dhclient', ['wlan0']);

    _create_apProcess = await Process.start('create_ap',
        ['-m', 'nat', 'wlan0', 'wlan0', 'MyAccessPoint', 'heslo1234']);

    _create_apProcess.stdout
        .listen((List<int> data) => myPrint(String.fromCharCodes(data).trim()));
  }

  @override
  Future<void> stop() async {
    await _wpa_supplicantProcess?.kill(ProcessSignal.sigint);
    await sleep1000();
    await _create_apProcess?.kill(ProcessSignal.sigint);
    await sleep1000();
    await sleep1000();
  }
}
