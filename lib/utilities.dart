import 'dart:io';

void myPrint(Object object) {
  print('${DateTime.now()}: $object');
}

Future sleep1000() {
  return Future.delayed(const Duration(milliseconds: 1000), () => '1');
}

Future<List<String>> getConnectedHosts() async {
  //iw dev ap0 station dump | grep Station | cut -d" " -f2
  final result = await Process.run('bash',
      ['-c', '"iw dev ap0 station dump | grep Station | cut -d\' \' -f2"']);

  if (result.exitCode != 0) {
    return <String>[];
  }

  return (result.stdout as String)
      .split('\n')
      .where((str) => str == '' ? false : true)
      .map((str) => str.trim())
      .toList();
}
